[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Call for Papers"
  lang = "en"
%]
            <h2>Call for Papers</h2>


<p>The Debconf team is excited to announce that we are now accepting proposals for contributions to this year's Debian conference! We invite you to submit proposals for how you would contribute until the deadline: May 1st, 2010, 23h59 UTC.
</p><p>There are many ways you can contribute, you could present a technical paper, host a panel discussion, put on a tutorial, do a performance, an art installation, a debate, host a meeting (BoFS, or Birds of a Feather Session), or other possibilities that you devise. This year we are also accepting proposals for tracks—a thematic grouping around a particular subject, and people to coordinate those tracks. If you are looking for ideas of things that you could contribute, or have ideas for things that you would like to see happen at Debconf, have a look at the <a href="http://wiki.debconf.org/wiki/DebConf10/Contribution_Brainstorm">Contribution Brainstorm page</a>.
</p><p>DebConf talks will be broadcast live on the Internet where possible, unless otherwise requested. Videos of the talks will be published on the web along with the presentation slides and papers. 
</p>
<h3>Step 1. Submit Proposal</h3>
<p>Proposals should provide an overview of your proposed contribution, similar to an abstract, and be no more than 600 words. Since contributions can cover a range of issues, please describe what your proposal covers, be it political (Free Software, law, advocacy, access, etc.), technical (d-i, OpenPGP, etc.), social (Debian structures and groups). It's okay to hit all three of these. Your proposal should also include some information on why you're qualified to present, or coordinate this topic. For example, you can include links to posts you have made to Debian mailing lists or other material that indicates your qualifications. Proposals and their abstracts will be accepted until May 1st, 2010, 23h59 UTC. 
</p>
<p>
To submit your proposal, go to <a href="http://debconf10.debconf.org/register.xhtml" title="http://debconf10.debconf.org/register.xhtml" rel="nofollow">http://debconf10.debconf.org/register.xhtml</a> and register as an attendee. Once you are registered, there is a "New Event/Paper" link. You will see your proposal on the site. You can choose between different presentation types. If you are unsure what type you should use, please read our <a href="http://wiki.debconf.org/wiki/DebConf10/TalkGlossary">definitions</a> first. If it's not there for some reason, or you have questions, please contact us immediately at talks@debconf.org.

</p>
<h3>Step 2. Committee Review</h3>
<p>The review committee for this year will make its decision by May 20th, 2010.  All correspondence will be done by email.
</p>
<h3>Step 3. Paper Submission</h3>

<p>Like last year, we are asking for papers to be submitted along with the presentations. A full paper is optional, however we strongly encourage you to submit one if possible as we intend to provide written information along with transcripts of the sessions for later viewing. Having written papers in advance will allow us to get translations done to help non-native-English speakers feel more comfortable with the topics presented.
Papers are due by July 1st, 2010, and should cover the topic in reasonable depth (3 pages A4 text, plus pictures and diagrams). We will use LaTeX to typeset the proceedings. Please submit your paper formatted in LaTeX. Should you be unfamiliar with LaTeX earlier submission in plain text is also fine.
</p>
<h3>Step 4. Live Presentation</h3>
<p>Longer presentations may have a break in the middle and should include workshop items that directly involve the participants. If using slides or any other presentation, please consider that your audience will consist of people who use free software, and your choice of application to prepare and display the slides should reflect this if at all possible.
</p>
<h3>Fine Print Publication Rights</h3>

<p>Debconf requires non-exclusive publication rights to papers, presentations, and any additional handouts or audio/visual materials used in conjunction with the presentation. The authors have the freedom to pick a DFSG-free license for the papers themselves and retain all copyrights.
The presentations will be recorded, and may be broadcast over the Internet. Any copies of the presentation will be made available under a license like the MIT/X11 license.
</p>
<h3>Failure to Submit</h3>
<p>In the event that a deadline is missed we reserve the right to revoke any offer to present.
</p>

<h3>About Debconf</h3>

<p>The annual Debconf conference is a technical and social forum for Debian developers, sponsors, affiliates, and friends. It allows various groups within Debian a chance to come together, network, and share their work.
</p>
