[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Getting to New York City"
  lang = "en"
%]

<h2>Getting to New York City</h2>


<p>New York City is served by the major airports John F. Kennedy
International (JFK), Newark Liberty International (EWR), and LaGuardia
(LGA).  There are smaller regional airports nearby as well.  All New
York City area airports can be searched using the IATA code "NYC".
All airports are connected to the city via public transportation running
24 hours/day, 7 days/week.</p>

<p>Penn Station is the primary long-range train station and one of the commuter train destinations, with Grand
Central Terminal and several other subway stations providing additional connections from commuter rail. Most incoming regional bus service arrives at or near the Port Authority Bus Terminal, or in Chinatown in downtown Manhattan.</p>

<p>Columbia University is on the New York City Subway 1 train, at
  116th street.  The M60 bus provides direct service to LaGuardia
  airport.  Connections to Newark Liberty International Airport are
  available through New Jersey Transit, and connections to John
  F. Kennedy International Airport are available through the New York
  City Subway or the Long Island Railroad.  Train stations are
  connected to Columbia via the New York City Subway.</p>

<p>The directions given on Columbia University's student affairs
website might be
useful <a href="http://www.studentaffairs.columbia.edu/admissions/visiting/directions.php">http://www.studentaffairs.columbia.edu/admissions/visiting/directions.php</a>.</p>

<ul>
	<li><a href="http://wikitravel.org/en/New_York_City#Get_in">Extremely comprehensive directions to New York City</a> from Wikitravel, covering all modes of transportation other than personal car travel.</li>
	<li>Wikipedia has detailed information
	on <a href="http://en.wikipedia.org/wiki/Transportation_to_New_York_City_area_airports">transportation
	between New York City and its surrounding airports</a>.</li>
	<li><a href="http://wiki.debconf.org/wiki/DebConf10/Travel">DebConf10
	Wiki travel page</a> (not created yet)</li>
	<li><a href="http://wiki.debconf.org/wiki/DebConf10/TravelCoordination">Coordinate</a> travel plans with others.</li>
	<li>The <a href="venue.xhtml[% langext %]">[%
t.leftmenu.venue %]</a> page contains several useful maps for
	  transportation within New York City.</li>
</ul>

<p>Unfortunately, the DebConf organizers will not be able to provide
 parking for attendees, including those staying in our residence
 halls.  The DebConf organizers highly recommend not driving into
 Manhattan - if you must drive, consider parking outside the city and
 taking mass transit in.  Understand that driving in the city is
 extremely frustrating, time-consuming, and parking is expensive.</p>

<p>There is sufficient bike parking for attendees in the area, and
 on-campus.</p>

<h3>In New York City</h3>

<p>There is an incredible amount of New York City travel and tourism
  information available on the Internet, including <a href="http://nycgo.com/">the city's official tourism website</a>.</p>
