[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Practical Information"
  lang = "en"
%]

<h2>Practical Information</h2>

<h3>What to Bring</h3>

Please visit the <a href="http://wiki.debconf.org/wiki/DebConf10/Welcome">Welcome page</a> for a general overview of the things you should be sure to bring, and the things that you need to know.

<h3>Getting to the Venue</h3>

Please visit the <a href="http://wiki.debconf.org/wiki/DebConf10/TravelInfo">Travel Information page</a> for logistical travel information from various locations to the venue.

<h3>Time</h3>

<p>In summer, the Eastern U.S. (including New York City) is on UTC-4 (EDT).</p>

<h3>Money</h3>

<p>The currency in the United States of America is the dollar.  There are many
ATMs throughout New York City, as well as at the airports and train stations
you may pass through on the way there. ATMs in New York will typically charge
US cardholders not affiliated with the ATM owner $1-3 for a withdrawal, but are
not allowed to charge the fee to international cardholders.</p>

<!-- where is the nearest ATM? -->

<p>Sales tax in New York State is rarely included in the prices you see, except
at corner stores, newsstands, and street vendors. In most cases you will be
charged an additional 8.875% when making a purchase. The generally-accepted
standard rate for tipping in restaurants is 15-20% in the case of satisfactory
service, so doubling the tax listed on the bill is a common calculation
technique.  It is customary to tip taxi drivers a comparable amount and
bartenders $1-2 per drink.</p>

<p>Most taxicabs now accept credit cards as a method of payment, though the cab drivers appreciate a generous tip in this case since they must pay a 5% processing fee for credit card payments.</p>

<h3>Weather</h3>

<p>Average temperatures:</p>

<table>
<tr><td>Month</td><td>Avg. low</td><td>Avg. high</td></tr>
<tr><td>June</td><td>17°C</td><td>26°C</td></tr>
<tr><td>July</td><td>21°C</td><td>29°C</td></tr>
<tr><td>August</td><td>20°C</td><td>28°C</td></tr>
</table>

<!-- update for more exact dates -->

<p><a href="http://www.wunderground.com/US/NY/New_York.html">Weather forecast for New York City</a></p>


<h3>Alcohol and tobacco</h3>

<p>The legal minimum age for buying alcohol is 21, and tobacco 18.  It is not
abnormal to be asked to show proof of age when making a purchase or entering
certain bars or clubs. People without US ID are advised to have their passports
with them when purchasing these products as store clerks are generally
unfamiliar with most foreign ID.</p>

<p>It is illegal to drive a car with a blood alcohol concentration of more than 0.08%.</p>

<p>Smoking is not allowed indoors in public places (including our venue's buildings), with very few exceptions.</p>

<h3>Electricity</h3>

<p><a href="http://en.wikipedia.org/wiki/Domestic_AC_power_plugs_and_sockets#Type_B">Type A and B power sockets</a> are used.</p>

<p>Electricity in the US is 110-120 V, 60 Hz.  Most outlets accept type A and B plugs.</p>


<h3>Telephone</h3>

<p>To call numbers in the U.S. from other countries, dial +1.</p>

<p>The international dialing prefix for calling other countries from the U.S. (for which + is a placeholder) is 011.</p>

<p>Mobile (cell) phone networks in the U.S. use the GSM standard on 850 MHz and 1900 MHz, or CDMA on 1900 MHz.</p>

<p>The emergency services can be reached by the American standard number, 911,
or via 112 from a GSM phone in accordance with the GSM standard.</p>

<h3>Health</h3>

<p>Tap water is piped in from the Croton Watershed and is clean and safe to drink.</p>

<p>No vaccinations or immunizations are needed to visit the US. <!-- TODO:
Find out requirements the US might have for travelers from certain foreign
countries to get vaccinations or e.g. yellow fever-related certificates.--></p>


<h3>Tourist information</h3>

<p>The official NY state tourism website is at <a href="http://www.iloveny.com/">http://www.iloveny.com/</a>. The corresponding NYC website is at <a href="http://nycgo.com/">http://nycgo.com/</a>.</p>
