[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Getting a visa for the United States"
  lang = "en"
%]

<h2>Getting a visa for the United States</h2>

  <p>The Debconf10 team is here to make getting permission to enter the United States as easy as possible. Among the services we will provide are preparing <em>invitation letters</em>, explaining <em>methods of gaining permission to enter</em>, and giving helpful <em>application tips</em>.</p>
  <p>Remember, the advice on this page applies only if you are applying for permission to visit the United States for the sole purpose of attending DebConf10. If you are seeking to enter for any other purpose, even if it is in addition to attending DebConf10, these directions may not be accurate. Contact our immigration counsel if you have questions.</p>
  <h2>Preface</h2>
  <p>Immigration law is one of the most complicated areas of law in the United States, and it also happens to be a hot political issue at the moment.</p>
  <p>Despite all of that, <em>the United States welcomes visitors</em>. The U.S. absolutely depends on - and welcomes - foreign visitors. There is a huge gap between immigration practices and political rhetoric about immigration. If you can convince the U.S. that you don't intend to stay, it will gladly grant you permission to enter.</p>
  <p>Most attendees should have no problem entering the U.S. for DebConf10. In Fiscal Year 2008, the <a href="http://www.dhs.gov/ximgtn/statistics/publications/YrBk08NI.shtm">U.S. granted</a> over 35 million requests for permission to enter. Of that number, over 17.5 million were granted through the Visa Waiver Program (VWP).</p>
  <h2>WARNING</h2>
  <p>Do not make any misrepresentations, even honest ones, on any form or to any U.S. government official. Such misrepresentations can be grounds to deny you a visa or permanent residence in the United States at any point in the future.</p>
  <p>If you have any questions, ask our immigration counsel.</p>
  <h1>Passports</h1>
  <p>You will need a passport to enter the United States. Make sure you have a <em>passport that is valid through at least six months after Debconf10</em>. If your current passport expires anytime before February 2011, apply for a new one immediately.</p>
  <p>The U.S. requires that all visitors entering via VWP have machine-readable passports (MRP). MRP have the two lines of text at the bottom of the biographical information page. In addition, nationals of the Czech Republic, Estonia, Hungary, Latvia, Lithuania, Malta, the Republic of Korea, and the Slovak Republic require passports with an RFID chip.</p>
  <h1>A step-by-step guide to getting permission to enter the U.S. for DebConf10</h1>
  <h2>Navigate U.S. immigration law in 24 easy steps!</h2>
  <p>How to get here largely depends on your citizenship. If you are a citizen of multiple countries, feel free to pick the method that looks easiest to enter.</p>
  <h2>Individual Country Exceptions</h2><em>If you are a citizen of Canada</em>
  <p>You generally will not need a visa to enter the United States. However, a permanent resident of Canada must apply for a visa, unless s/he is a national of a Visa Waiver Program country.</p><em>If you are a citizen of Mexico</em>
  <p>You will need a visa (see "Getting a Visa") or a <a href="http://travel.state.gov/visa/temp/types/types_1266.html">Border Crossing Card</a>, or "laser visa." If you have a Border Crossing Card already, you can use it to come to Debconf10. If you don't yet have a Border Crossing Card, it's your choice whether to apply for one, which will last ten years, or to apply for a regular visa, which will last far less than that.</p><em>If you are a citizen of Bermuda</em>
  <p>Citizens of the British Overseas Territories of Bermuda will not need a visa to enter the U.S. to attend DebConf10.</p><em>If you are a citizen of the Bahamas</em>
  <p>Bahamian citizens do not require a visa to enter the United States if they apply for entry at one of the Preclearance Facilities located in Nassau or Freeport International Airports.</p><em>If you are a citizen of the Federated States of Micronesia, Marshall Islands, and Palau</em>
  <p>Citizens of the Federated States of Micronesia, the Republic of the Marshall Islands, and the Republic of Palau (except for adopted children), may enter, reside, study, and work indefinitely in the United States without visas.</p>
  <h2>The Visa Waiver Program</h2>
  <p>If you are a national of one of the 36 Visa Waiver Program (VWP) countries, you may complete a short online application that, once approved, will allow you to visit the United States for up to 90 days without a visa.</p>
  <p>The 36 VWP countries are: Andorra, Austria, Australia, Belgium, Brunei, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Iceland, Ireland, Italy, Japan, Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Monaco, the Netherlands, New Zealand, Norway, Portugal, San Marino, Singapore, Slovak Republic, Slovenia, South Korea, Spain, Sweden, Switzerland, and the United Kingdom*.</p>
  <p>*United Kingdom Passports: Only United Kingdom passports notated with "British Citizens" and/or "with unrestricted right of abode in the United Kingdom" are eligible for VWP admission. Holders of passports indicating that the bearer is a British Subject, British Dependent Territories Citizen, British Overseas Citizen or British National (Overseas) do not qualify for VWP travel.</p>
  <p>Your passport must meet <a href="http://travel.state.gov/visa/temp/without/without_1990.html#mrprequirements">certain requirements</a> to qualify for VWP.</p>
  <p>There are also other, narrow restrictions on eligibility for VWP. See "Other Issues"</p>
  <p>If you believe you qualify for VWP, you must fill out the online <a href="https://esta.cbp.dhs.gov/">Electronic System for Travel Authorization</a> (ESTA) form.</p>
  <p>If you have any questions about the ESTA form, do not "guess." E-mail our immigration counsel. See the "Warning" section.</p>
  <p>Once completed, most applications will be approved immediately. So, <em>apply now</em>. If your VWP application is denied, you must apply for a visa. You can always amend the information (e.g. flight info, home address) later.</p>
  <p>VWP authorization is valid for two years. Thus, if you have traveled to the United States within the past year under the VWP, you may be able to attend DebConf10 without reapplying.</p>
  <h2>Applying for a Visa</h2>
  <p>To apply for a B1/B2 visa to enter the United States, follow these steps:</p>
  <ul>
    <li>Apply for an interview at your local <a href="http://www.usembassy.gov/">U.S. consulate or embassy</a></li>
    <li style="list-style: none">
      <ul>
        <li>Contact them and ask what the soonest time available is for an interview for a B1/B2 visa</li>
        <li>Ask them what to bring to the interview.  Each consular office has different procedures, and many use different forms.  For example, some consular offices use the <a href="https://ceac.state.gov/genniv/"DS-160 online, Nonimmigrant Visa Electronic Application</a>.  Other consular offices will use the DS-156, <a href="http://evisaforms.state.gov/">which you can fill out online and print</a>.  Ask which forms to use.</li>
        <li>Ask about bringing a photograph and getting fingerprinted.  All visa applicants must submit a photo and fingerprints, and each consular office will have different procedures.</li>
      </ul>
    </li>
    <li>Gather documents that prove your ties to where you live now</li>
    <li style="list-style: none">
      <ul>
        <li>Bank records and real estate holdings are stronger proof</li>
        <li>If you have a wife or children, bring proof of them and state that they will remain (if that's true)</li>
      </ul>
      <ul>
        <li>Evidence of enrollment in a degree program or family ties are not very persuasive</li>
        <li>Be creative. If you are the sole caregiver for a sick family member, bring proof of that.</li>
        <li>Any kind of documentation will work. Make sure copies are clear. Make sure written statements by others are sworn and signed.</li>
        <li>The officer interviewing you will look for proof that you do not intend to remain in the U.S. There are countless ways to prove this, just be sure to bring documentation.</li>
      </ul>
    </li>
    <li>Sitting for the interview</li>
    <li style="list-style: none">
      <ul>
        <li>When interviewing, dress as if it were a business meeting.</li>
        <li>Some embassies may allow you to post a bond. Ask the interviewing officer about that option.</li>
        <li>Ask for feedback. If the officer seems skeptical, ask what else you can provide. Be courteous!</li>
      </ul>
    </li>
    <li>Provide a letter from the event sponsor inviting you</li>
    <li style="list-style: none">
      <ul>
        <li>E-mail visa@debconf.org with your full name, address, and passport number and we'll forward you a letter to provide to the interviewing officer.</li>
        <li>Please send the information in a text file attachment <em>following the format of <a href="http://lawfgb.com/dc10-sample.txt">this sample</em></a>.
      </ul>
    </li>
  </ul>
  <p><em>Some embassies have <a href="http://travel.state.gov/visa/temp/wait/tempvisitors_wait.php">significant waits for interviews</a>.</em> In certain cases, you may need to schedule more than one appointment to supplement your application. Schedule as soon as you assemble your supporting documents!</p>
  <p>If you are approved, the embassy or consulate will inform you about what further steps to take, including how to make payment of the required fee.</p>
  <p>If you are denied a visa after an interview for any reason, contact our immigration counsel. Be sure to keep every piece of paper they give you!</p>
  <h2>Picking a Port of Entry</h2><em>Booking your flight</em>
  <ul>
    <li>You will clear immigration and customs at the port of entry if you are not entering the US from a preclearance airport. If you fly into Atlanta and connect there to a flight to New York, you will clear immigration in Atlanta.</li>
    <li>Since all entry permissions will be set in advance of arrival, your entry should go smoothly</li>
    <li>If practical, try to fly directly into LaGuardia, Newark Liberty (just a few miles from Manhattan), or JFK Airport</li>
    <li>In the highly unlikely event of problems entering, free legal help will be available in New York</li>
  </ul><em>Preclearance: a way to reduce uncertainty</em>
  <ul>
    <li><a href="http://en.wikipedia.org/wiki/United_States_border_preclearance">Preclearance</a> is a program implemented at several airports in Canada, Aruba, The Bahamas, Bermuda, and Ireland where U.S. officers screen you for entry to the U.S. before you board the plane to the U.S. instead of after landing</li>
    <li>If you are at all uncertain or apprehensive about border inspection at a U.S. airport, get a flight from a preclearance city</li>
    <li>Foreign and not U.S. law will apply, but...</li>
    <li>...passengers departing from a preclearance airport must meet the same requirements for entry as they would entering the U.S. any other way</li>
  </ul>
  <h2>Other Important Issues</h2>
  <p>If you think have been convicted of a crime involving "<a href="http://en.wikipedia.org/wiki/Moral_turpitude">moral turpitude</a>," contact our immigration counsel. Crimes of moral turpitude (CMT) are a highly contentious issue in immigration law, and the definition is refined and adapted regularly.</p>
  <h2>Our Immigration Lawyer</h2>
  <p><a href="http://www.lawfgb.com">Franklin Bynum</a> is an attorney and free software enthusiast. He is licensed to practice in the states of New York and New Jersey and is thus eligible to appear before immigration courts across the U.S. He is a member of the National Lawyer's Guild's National Immigration Project. He will also be in New York for the event.</p>
  <p>He is available for consultations over e-mail. To reach him:</p>
  <ul>
    <li>Please consult materials online first</li>
    <li>If you still need help, contact visa@debconf.org</li>
    <li>Do not include many details in the first message, simply say who you are and that you need assistance.</li>
  </ul>
  <h2>IMPORTANT FINAL NOTE</h2>
  <p>Before departing for the United States for DebConf10, carry the following number on your person (i.e. in your pocket): 888-744-2480. In the highly unlikely event you have trouble at the border, ask to call that number to reach Frank. Even though problems are <em>highly</em> unlikely, be sure to carry this number!</p>
  <h2>Other Resources</h2>
  <p><a href="http://travel.state.gov/visa/temp/without/without_1990.html">U.S. Department of State VWP Page</a>
  <a href="http://www.cbp.gov/xp/cgov/travel/id_visa/business_pleasure/vwp/vwp.xml">U.S. Customs and Border Protection VWP FAQ</a></p>
