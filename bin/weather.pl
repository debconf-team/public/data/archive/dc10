#!/usr/bin/perl

use warnings;
use strict;
use Geo::METAR;
use LWP::UserAgent;

my $URL = "http://weather.noaa.gov/pub/data/observations/metar/stations/KNYC.TXT"; 
my $ua = LWP::UserAgent->new;
$ua->timeout(10);
$ua->env_proxy;
my $response = $ua->get($URL);
unless($response->is_success) {
	die $response->status_line;
}

my $m = Geo::METAR->new();
$m->metar($response->content);

printf("Updated %s<br/>\n".
	"%s %d °C (%d °F)<br/>\n".
	"%d hPa (%.2f in.Hg)<br />\n".
	"Wind: %s<br/>\n%d km/h (%d mph)\n",
	$m->TIME(), join(" ", @{$m->WEATHER()}),
	$m->TEMP_C() * 1, $m->TEMP_F() * 1, $m->ALT_HP(), $m->ALT(),
	$m->WIND_DIR_ENG(), $m->WIND_MS() * 3.6, $m->WIND_MPH());

