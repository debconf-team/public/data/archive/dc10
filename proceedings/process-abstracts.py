#!/usr/bin/python
# emacs: -*- mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vi: set ft=python sts=4 ts=4 sw=4 et:

import csv
import codecs
import sys
import re

sys.stdout = codecs.getwriter('utf8')(sys.stdout)
sys.stdin = codecs.getreader('utf8')(sys.stdin)

fields = '''title
subtitle
speakers
submitters
coordinators
event_state
event_state_progress
conference_track
event_type
public
paper
slides
abstract
description
id
'''.split()

def selectTalk(talk):
    return (talk['event_state'] == 'accepted' 
        and talk['event_state_progress']=='confirmed')

def sortKey(talk):
    return "%-10s %-30s" % (talk['conference_track'] , 
                          talk['title'])

speaker_re = re.compile('(?P<name>.*?) *<(?P<email>.*)>')
def format_speaker(x):
    xre = speaker_re.match(x.strip())
    if xre:
        return ur'\href{mailto:%(name)s <%(email)s>}{%(name)s $\langle$%(email)s$\rangle$}' % xre.groupdict()
    else:
        return x

url_re = re.compile(ur'([]\n (]|^)(https?://\S+)', flags=re.UNICODE)
items_re = re.compile(ur'\n *\*([^\n]*)', flags=re.UNICODE)
def format_abstract(x):
    # Enclose all http:// into \url
    x = url_re.sub(u'\\1\\url{\\2}', x)
    # TODO: take care about trailing references
    # TODO: itemize lists of lines starting with *
    #  for now silly way
    x = items_re.sub(ur'\n - \1\\\\\n', x)
    return x

def format(talk):
    return ur'''
\section{\href{http://penta.debconf.org/dc10_schedule/events/%s.en.html}{%s}}
\begin{flushright}
%s
\end{flushright}
%s
''' % (
    talk['id'], talk['title'],
    ur'\\'.join([format_speaker(x) for x in talk['speakers'].split(',')]),
    format_abstract(talk['abstract']))

# dear python, it is 2010. you suck.
# yoh: 3.1.2 is in Debian already ;-) might be a good place to test? ;-)

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.DictReader(utf_8_encoder(unicode_csv_data),
                                dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield dict([(pair[0], unicode(pair[1], 'utf-8')) for pair in row.items()])

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')

reader = unicode_csv_reader(sys.stdin,fieldnames=fields)
                        
talks =sorted(filter(selectTalk,reader),key=sortKey)

track=''
last_track=''
# print ur'\chapter{Abstracts}'
print ur'\chapter{General}'

for talk in talks:
    track=talk['conference_track']
    if (track !=last_track):
        print ur'\chapter{%s}' % track
        last_track=track
    
    print format(talk)
                

    
