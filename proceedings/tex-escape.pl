#!/usr/bin/perl

while(<>){
  s/\r\n/\n/;
  s/_/\\_/g;
  s/&/\\&/g;
  s/\^/\\^/g;
  s/#/\\#/g;
  print;
}
