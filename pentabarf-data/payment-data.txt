Thu Aug 19 22:10:24 2010

====================
Statistics for all people
Total attendee fees: 127597

 6500  (  5)  Corporate registration
 1300  (  1)  Corporate registration, DebConf only
   90  (  3)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
  106  (  2)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
    0  (  2)  Food for 0 days
  120  (  4)  Food for 1 days
  600  (  2)  Food for 10 days
  330  (  1)  Food for 11 days
  780  (  2)  Food for 13 days
 4200  ( 10)  Food for 14 days
   60  (  1)  Food for 2 days
  540  (  6)  Food for 3 days
  840  (  7)  Food for 4 days
  450  (  3)  Food for 5 days
 3780  ( 21)  Food for 6 days
 4200  ( 20)  Food for 7 days
46800  (195)  Food for 8 days
  540  (  2)  Food for 9 days
20800  ( 32)  Professional registration
 7800  (  6)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
 2968  (  4)  Room for 14 days
  106  (  1)  Room for 2 days
  318  (  2)  Room for 3 days
 1272  (  6)  Room for 4 days
  265  (  1)  Room for 5 days
 2862  (  9)  Room for 6 days
 3339  (  9)  Room for 7 days
14840  ( 35)  Room for 8 days
  477  (  1)  Room for 9 days



====================
Statistics for ONLY people who have both dates in Penta
Total attendee fees: 69137

 6500  (  5)  Corporate registration
 1300  (  1)  Corporate registration, DebConf only
   90  (  3)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
  106  (  2)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
    0  (  2)  Food for 0 days
  120  (  4)  Food for 1 days
  600  (  2)  Food for 10 days
  330  (  1)  Food for 11 days
  780  (  2)  Food for 13 days
  840  (  2)  Food for 14 days
   60  (  1)  Food for 2 days
  540  (  6)  Food for 3 days
  840  (  7)  Food for 4 days
  300  (  2)  Food for 5 days
 3780  ( 21)  Food for 6 days
 3780  ( 18)  Food for 7 days
 6000  ( 25)  Food for 8 days
  540  (  2)  Food for 9 days
19500  ( 30)  Professional registration
 6500  (  5)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
  742  (  1)  Room for 14 days
  106  (  1)  Room for 2 days
  318  (  2)  Room for 3 days
 1272  (  6)  Room for 4 days
  265  (  1)  Room for 5 days
 2862  (  9)  Room for 6 days
 3339  (  9)  Room for 7 days
 5936  ( 14)  Room for 8 days
  477  (  1)  Room for 9 days



====================
Statistics for sponsored people
Total attendee fees: 2997

   30  (  1)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
   53  (  1)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
  600  (  2)  Food for 10 days
  420  (  2)  Food for 7 days
  960  (  4)  Food for 8 days
  270  (  1)  Food for 9 days



====================
Statistics for UNsponsored people
Total attendee fees: 124600

 6500  (  5)  Corporate registration
 1300  (  1)  Corporate registration, DebConf only
   60  (  2)  DebCamp food for 1 days
   53  (  1)  DebCamp room for 1 days
    0  (  2)  Food for 0 days
  120  (  4)  Food for 1 days
  330  (  1)  Food for 11 days
  780  (  2)  Food for 13 days
 4200  ( 10)  Food for 14 days
   60  (  1)  Food for 2 days
  540  (  6)  Food for 3 days
  840  (  7)  Food for 4 days
  450  (  3)  Food for 5 days
 3780  ( 21)  Food for 6 days
 3780  ( 18)  Food for 7 days
45840  (191)  Food for 8 days
  270  (  1)  Food for 9 days
20800  ( 32)  Professional registration
 7800  (  6)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
 2968  (  4)  Room for 14 days
  106  (  1)  Room for 2 days
  318  (  2)  Room for 3 days
 1272  (  6)  Room for 4 days
  265  (  1)  Room for 5 days
 2862  (  9)  Room for 6 days
 3339  (  9)  Room for 7 days
14840  ( 35)  Room for 8 days
  477  (  1)  Room for 9 days



