Thu Aug 19 22:10:36 2010

====================
Statistics for all people
Total attendee fees: 179679

 6500  (  5)  Corporate registration
 1300  (  1)  Corporate registration, DebConf only
   90  (  3)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
  106  (  2)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
    0  (  2)  Food for 0 days
  150  (  5)  Food for 1 days
  900  (  3)  Food for 10 days
  330  (  1)  Food for 11 days
  720  (  2)  Food for 12 days
 1170  (  3)  Food for 13 days
 6720  ( 16)  Food for 14 days
  120  (  2)  Food for 2 days
  540  (  6)  Food for 3 days
  840  (  7)  Food for 4 days
  450  (  3)  Food for 5 days
 4860  ( 27)  Food for 6 days
 5460  ( 26)  Food for 7 days
64800  (270)  Food for 8 days
  540  (  2)  Food for 9 days
23400  ( 36)  Professional registration
 7800  (  6)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
   53  (  1)  Room for 1 days
  530  (  1)  Room for 10 days
 1272  (  2)  Room for 12 days
  689  (  1)  Room for 13 days
 7420  ( 10)  Room for 14 days
  106  (  1)  Room for 2 days
  318  (  2)  Room for 3 days
 1272  (  6)  Room for 4 days
  265  (  1)  Room for 5 days
 3816  ( 12)  Room for 6 days
 4823  ( 13)  Room for 7 days
30528  ( 72)  Room for 8 days
  477  (  1)  Room for 9 days



====================
Statistics for ONLY people who have both dates in Penta
Total attendee fees: 93245

 6500  (  5)  Corporate registration
 1300  (  1)  Corporate registration, DebConf only
   90  (  3)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
  106  (  2)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
    0  (  2)  Food for 0 days
  150  (  5)  Food for 1 days
  900  (  3)  Food for 10 days
  330  (  1)  Food for 11 days
  720  (  2)  Food for 12 days
 1170  (  3)  Food for 13 days
 2520  (  6)  Food for 14 days
  120  (  2)  Food for 2 days
  540  (  6)  Food for 3 days
  840  (  7)  Food for 4 days
  300  (  2)  Food for 5 days
 4860  ( 27)  Food for 6 days
 5040  ( 24)  Food for 7 days
 9600  ( 40)  Food for 8 days
  540  (  2)  Food for 9 days
21450  ( 33)  Professional registration
 6500  (  5)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
   53  (  1)  Room for 1 days
  530  (  1)  Room for 10 days
 1272  (  2)  Room for 12 days
  689  (  1)  Room for 13 days
 3710  (  5)  Room for 14 days
  106  (  1)  Room for 2 days
  318  (  2)  Room for 3 days
 1272  (  6)  Room for 4 days
  265  (  1)  Room for 5 days
 3816  ( 12)  Room for 6 days
 4823  ( 13)  Room for 7 days
11024  ( 26)  Room for 8 days
  477  (  1)  Room for 9 days



====================
Statistics for sponsored people
Total attendee fees: 3807

   30  (  1)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
   53  (  1)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
  600  (  2)  Food for 10 days
  360  (  2)  Food for 6 days
  630  (  3)  Food for 7 days
 1200  (  5)  Food for 8 days
  270  (  1)  Food for 9 days



====================
Statistics for UNsponsored people
Total attendee fees: 175872

 6500  (  5)  Corporate registration
 1300  (  1)  Corporate registration, DebConf only
   60  (  2)  DebCamp food for 1 days
   53  (  1)  DebCamp room for 1 days
    0  (  2)  Food for 0 days
  150  (  5)  Food for 1 days
  300  (  1)  Food for 10 days
  330  (  1)  Food for 11 days
  720  (  2)  Food for 12 days
 1170  (  3)  Food for 13 days
 6720  ( 16)  Food for 14 days
  120  (  2)  Food for 2 days
  540  (  6)  Food for 3 days
  840  (  7)  Food for 4 days
  450  (  3)  Food for 5 days
 4500  ( 25)  Food for 6 days
 4830  ( 23)  Food for 7 days
63600  (265)  Food for 8 days
  270  (  1)  Food for 9 days
23400  ( 36)  Professional registration
 7800  (  6)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
   53  (  1)  Room for 1 days
  530  (  1)  Room for 10 days
 1272  (  2)  Room for 12 days
  689  (  1)  Room for 13 days
 7420  ( 10)  Room for 14 days
  106  (  1)  Room for 2 days
  318  (  2)  Room for 3 days
 1272  (  6)  Room for 4 days
  265  (  1)  Room for 5 days
 3816  ( 12)  Room for 6 days
 4823  ( 13)  Room for 7 days
30528  ( 72)  Room for 8 days
  477  (  1)  Room for 9 days



