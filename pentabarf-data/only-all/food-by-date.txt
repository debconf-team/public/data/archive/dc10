Thu Aug 19 22:10:54 2010

Assuming everyone wants meals on the first day, and no meals on the
last day.  Clearly, this will be a bit wrong on both ends.


====================
Statistics for all people
Number of people: 608
Total person-days: 4713
Assuming meals per day: 2
Total number of meals: 9426
Cost per meal: 15
Total cost: 141390
Regular:    471
Vegetarian: 59
Vegan:      20
Other:      19
NotWithUs:  39

                  Reg   Veg  Vegan Other   Total      None
2010-07-25(Sun)    45    10     2     5      62         2
2010-07-26(Mon)    55    12     2     5      74         2
2010-07-27(Tue)    60    13     2     6      81         2
2010-07-28(Wed)    66    14     2     6      88         2
2010-07-29(Thu)    68    14     2     6      90         2
2010-07-30(Fri)    77    15     2     6     100         3
2010-07-31(Sat)   394    44    19    18     475        31
2010-08-01(Sun)   451    57    20    19     547        38
2010-08-02(Mon)   453    57    20    19     549        38
2010-08-03(Tue)   457    57    19    19     552        37
2010-08-04(Wed)   457    58    19    19     553        36
2010-08-05(Thu)   456    58    19    19     552        36
2010-08-06(Fri)   448    57    18    19     542        37
2010-08-07(Sat)   371    46    15    16     448        34
2010-08-08(Sun)     0     0     0     0       0         0



====================
Statistics for ONLY people who have both dates in Penta
Number of people: 330
Total person-days: 2652
Assuming meals per day: 2
Total number of meals: 5304
Cost per meal: 15
Total cost: 79560
Regular:    265
Vegetarian: 35
Vegan:      9
Other:      10
NotWithUs:  11

                  Reg   Veg  Vegan Other   Total      None
2010-07-25(Sun)    36     9     1     5      51         1
2010-07-26(Mon)    46    11     1     5      63         1
2010-07-27(Tue)    51    12     1     6      70         1
2010-07-28(Wed)    57    13     1     6      77         1
2010-07-29(Thu)    59    13     1     6      79         1
2010-07-30(Fri)    68    14     1     6      89         2
2010-07-31(Sat)   191    20     8     9     228         4
2010-08-01(Sun)   246    33     9    10     298        11
2010-08-02(Mon)   248    33     9    10     300        11
2010-08-03(Tue)   251    33     8    10     302        10
2010-08-04(Wed)   251    34     8    10     303         9
2010-08-05(Thu)   250    34     8    10     302         9
2010-08-06(Fri)   242    33     7    10     292         9
2010-08-07(Sat)   165    22     4     7     198         6
2010-08-08(Sun)     0     0     0     0       0         0



====================
Statistics for (food) sponsored people (BAF,BF)
Number of people: 148
Total person-days: 1405
Assuming meals per day: 2
Total number of meals: 2810
Cost per meal: 15
Total cost: 42150
Regular:    114
Vegetarian: 22
Vegan:      5
Other:      7
NotWithUs:  0

                  Reg   Veg  Vegan Other   Total      None
2010-07-25(Sun)    25     8     1     4      38         0
2010-07-26(Mon)    32    10     1     4      47         0
2010-07-27(Tue)    36    11     1     5      53         0
2010-07-28(Wed)    39    12     1     5      57         0
2010-07-29(Thu)    41    12     1     5      59         0
2010-07-30(Fri)    45    12     1     5      63         0
2010-07-31(Sat)    95    15     4     6     120         0
2010-08-01(Sun)   108    20     5     7     140         0
2010-08-02(Mon)   112    20     5     7     144         0
2010-08-03(Tue)   113    21     5     7     146         0
2010-08-04(Wed)   113    22     5     7     147         0
2010-08-05(Thu)   113    22     5     7     147         0
2010-08-06(Fri)   106    22     5     7     140         0
2010-08-07(Sat)    81    14     3     6     104         0
2010-08-08(Sun)     0     0     0     0       0         0



====================
Statistics for (food) UNsponsored people  !(BAF,BF)
Number of people: 460
Total person-days: 3308
Assuming meals per day: 2
Total number of meals: 6616
Cost per meal: 15
Total cost: 99240
Regular:    357
Vegetarian: 37
Vegan:      15
Other:      12
NotWithUs:  39

                  Reg   Veg  Vegan Other   Total      None
2010-07-25(Sun)    20     2     1     1      24         2
2010-07-26(Mon)    23     2     1     1      27         2
2010-07-27(Tue)    24     2     1     1      28         2
2010-07-28(Wed)    27     2     1     1      31         2
2010-07-29(Thu)    27     2     1     1      31         2
2010-07-30(Fri)    32     3     1     1      37         3
2010-07-31(Sat)   299    29    15    12     355        31
2010-08-01(Sun)   343    37    15    12     407        38
2010-08-02(Mon)   341    37    15    12     405        38
2010-08-03(Tue)   344    36    14    12     406        37
2010-08-04(Wed)   344    36    14    12     406        36
2010-08-05(Thu)   343    36    14    12     405        36
2010-08-06(Fri)   342    35    13    12     402        37
2010-08-07(Sat)   290    32    12    10     344        34
2010-08-08(Sun)     0     0     0     0       0         0



====================
Statistics for people paying for food day-by-day (B,BA,None)
Number of people: 411
Total person-days: 2920
Assuming meals per day: 2
Total number of meals: 5840
Cost per meal: 15
Total cost: 87600
Regular:    320
Vegetarian: 32
Vegan:      13
Other:      10
NotWithUs:  36

                  Reg   Veg  Vegan Other   Total      None
2010-07-25(Sun)    15     2     1     1      19         2
2010-07-26(Mon)    18     2     1     1      22         2
2010-07-27(Tue)    18     2     1     1      22         2
2010-07-28(Wed)    21     2     1     1      25         2
2010-07-29(Thu)    21     2     1     1      25         2
2010-07-30(Fri)    26     2     1     1      30         2
2010-07-31(Sat)   265    25    13    10     313        29
2010-08-01(Sun)   306    32    13    10     361        35
2010-08-02(Mon)   304    32    13    10     359        35
2010-08-03(Tue)   307    31    12    10     360        34
2010-08-04(Wed)   307    31    12    10     360        33
2010-08-05(Thu)   306    31    12    10     359        33
2010-08-06(Fri)   306    30    11    10     357        34
2010-08-07(Sat)   261    27    11     9     308        31
2010-08-08(Sun)     0     0     0     0       0         0



====================
Statistics for Corp/Prof attendees (C,P)
Number of people: 49
Total person-days: 388
Assuming meals per day: 2
Total number of meals: 776
Cost per meal: 15
Total cost: 11640
Regular:    37
Vegetarian: 5
Vegan:      2
Other:      2
NotWithUs:  3

                  Reg   Veg  Vegan Other   Total      None
2010-07-25(Sun)     5     0     0     0       5         0
2010-07-26(Mon)     5     0     0     0       5         0
2010-07-27(Tue)     6     0     0     0       6         0
2010-07-28(Wed)     6     0     0     0       6         0
2010-07-29(Thu)     6     0     0     0       6         0
2010-07-30(Fri)     6     1     0     0       7         1
2010-07-31(Sat)    34     4     2     2      42         2
2010-08-01(Sun)    37     5     2     2      46         3
2010-08-02(Mon)    37     5     2     2      46         3
2010-08-03(Tue)    37     5     2     2      46         3
2010-08-04(Wed)    37     5     2     2      46         3
2010-08-05(Thu)    37     5     2     2      46         3
2010-08-06(Fri)    36     5     2     2      45         3
2010-08-07(Sat)    29     5     1     1      36         3
2010-08-08(Sun)     0     0     0     0       0         0



