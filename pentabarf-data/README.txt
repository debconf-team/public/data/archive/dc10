This data is data extracted from Pentabarf.  It was constantly updated
before and during the conference for our planning information.

This is what Pentabarf says, but of course may not reflect reality,
since Pentabarf isn't always updated.

In particular, all the budget numbers are estimates and shouldn't be
taken as meaning that much.

There are many places where it divides among the different sponsorship
types:
B - basic (to sponsorship)
BF - food sponsorship
BA - accom sponsorship
BAF - all sponsored
P - professional
C - corporate

It can be confusing, but... given the confusingness of the
registration categories, there were times it helped us to figure
things out.  I apologize in advance to future years, but at least this
is more data than most years leave behind.
