Thu Aug 19 22:11:40 2010

====================
Statistics for all people
Number of people:      494
Total nights:          4081

Total room-nights:     2192
Total room cost:       116176
Total food-days:       3845
Total meal cost:       115350

Total cost:            231526

Fees, food:            63570
Fees, accom:           26977
Fees, professional:    29250
Fees, corporate:       7800
Fees, DebCamp:         0
Fees, other:           0

Total fees:            127597
Total fees (real):     42468.0

Net cost of attendees: 103929



====================
Statistics for sponsored people (BAF, BA, BF)
Number of people:      154
Total nights:          1453

Total room-nights:     1351
Total room cost:       71603
Total food-days:       1453
Total meal cost:       43590

Total cost:            115193

Fees, food:            2520
Fees, accom:           477
Fees, professional:    0
Fees, corporate:       0
Fees, DebCamp:         0
Fees, other:           0

Total fees:            2997
Total fees (real):     477.0

Net cost of attendees: 112196



====================
Statistics for unsponsored peolpe !(BAF, BA, BF, CORP, PROF)
Number of people:      295
Total nights:          2245

Total room-nights:     499
Total room cost:       26447
Total food-days:       2033
Total meal cost:       60990

Total cost:            87437

Fees, food:            60990
Fees, accom:           26447
Fees, professional:    0
Fees, corporate:       0
Fees, DebCamp:         0
Fees, other:           0

Total fees:            87437
Total fees (real):     9213.0

Net cost of attendees: 0



====================
Statistics for unsponsored peolpe (CORP, PROF)
Number of people:      45
Total nights:          383

Total room-nights:     342
Total room cost:       18126
Total food-days:       359
Total meal cost:       10770

Total cost:            28896

Fees, food:            60
Fees, accom:           53
Fees, professional:    29250
Fees, corporate:       7800
Fees, DebCamp:         0
Fees, other:           0

Total fees:            37163
Total fees (real):     32778.0

Net cost of attendees: -8267



