Thu Aug 19 22:12:21 2010

====================
Statistics for all people
Total attendee fees: 67011

 6500  (  5)  Corporate registration
   60  (  2)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
  106  (  2)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
   60  (  2)  Food for 1 days
  600  (  2)  Food for 10 days
  330  (  1)  Food for 11 days
  390  (  1)  Food for 13 days
 1260  (  3)  Food for 14 days
   60  (  1)  Food for 2 days
  360  (  4)  Food for 3 days
  600  (  5)  Food for 4 days
  150  (  1)  Food for 5 days
 2160  ( 12)  Food for 6 days
 2730  ( 13)  Food for 7 days
17040  ( 71)  Food for 8 days
  270  (  1)  Food for 9 days
18200  ( 28)  Professional registration
 6500  (  5)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
  742  (  1)  Room for 14 days
  106  (  1)  Room for 2 days
  159  (  1)  Room for 3 days
  848  (  4)  Room for 4 days
 1272  (  4)  Room for 6 days
 2226  (  6)  Room for 7 days
 2968  (  7)  Room for 8 days



====================
Statistics for ONLY people who have both dates in Penta
Total attendee fees: 53481

 6500  (  5)  Corporate registration
   60  (  2)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
  106  (  2)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
   60  (  2)  Food for 1 days
  600  (  2)  Food for 10 days
  330  (  1)  Food for 11 days
  390  (  1)  Food for 13 days
  840  (  2)  Food for 14 days
   60  (  1)  Food for 2 days
  360  (  4)  Food for 3 days
  600  (  5)  Food for 4 days
 2160  ( 12)  Food for 6 days
 2730  ( 13)  Food for 7 days
 4080  ( 17)  Food for 8 days
  270  (  1)  Food for 9 days
18200  ( 28)  Professional registration
 6500  (  5)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
  742  (  1)  Room for 14 days
  106  (  1)  Room for 2 days
  159  (  1)  Room for 3 days
  848  (  4)  Room for 4 days
 1272  (  4)  Room for 6 days
 2226  (  6)  Room for 7 days
 2968  (  7)  Room for 8 days



====================
Statistics for sponsored people
Total attendee fees: 2997

   30  (  1)  DebCamp food for 1 days
   60  (  1)  DebCamp food for 2 days
  180  (  1)  DebCamp food for 6 days
   53  (  1)  DebCamp room for 1 days
  106  (  1)  DebCamp room for 2 days
  318  (  1)  DebCamp room for 6 days
  600  (  2)  Food for 10 days
  420  (  2)  Food for 7 days
  960  (  4)  Food for 8 days
  270  (  1)  Food for 9 days



====================
Statistics for UNsponsored people
Total attendee fees: 64014

 6500  (  5)  Corporate registration
   30  (  1)  DebCamp food for 1 days
   53  (  1)  DebCamp room for 1 days
   60  (  2)  Food for 1 days
  330  (  1)  Food for 11 days
  390  (  1)  Food for 13 days
 1260  (  3)  Food for 14 days
   60  (  1)  Food for 2 days
  360  (  4)  Food for 3 days
  600  (  5)  Food for 4 days
  150  (  1)  Food for 5 days
 2160  ( 12)  Food for 6 days
 2310  ( 11)  Food for 7 days
16080  ( 67)  Food for 8 days
18200  ( 28)  Professional registration
 6500  (  5)  Professional registration, DebCamp+DebConf
  650  (  1)  Professional registration, DebConf only
  742  (  1)  Room for 14 days
  106  (  1)  Room for 2 days
  159  (  1)  Room for 3 days
  848  (  4)  Room for 4 days
 1272  (  4)  Room for 6 days
 2226  (  6)  Room for 7 days
 2968  (  7)  Room for 8 days



