Event types:
bof                 56
lecture             52
other               15
workshop            12
podium               6
keynote              4
lightning            4
contest              2
meeting              2
None                 0


====================
Event states:
accepted           135
rejected            18
undecided           10


====================
Event states and progress:
accepted           canceled             3
accepted           confirmed          132
rejected           confirmed           13
rejected           unconfirmed          5
undecided          new                 10


====================
Number of talks per person:
12  1
11  0
10  0
 9  0
 8  1
 7  1
 6  3
 5  4
 4  4
 3 10
 2 20
 1 54


====================
Number of talks by each person type:
Debian Developer                    126
DebConf Organizer                    37
Otherwise involved in Debian         17
Not yet involved but interested      11
DebConf Volunteer                     8
Sponsor                               5
 --- please select one ---            1


====================
Number of talks, by DebCamp attendance
I have a specific work plan for DebCamp  109
I won't be attending DebCamp              96
